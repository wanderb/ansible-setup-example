# Ansible host setup example

This is an example of how to configure an Ansible specific user when one does
not yet exist. It assumes that your current _local_ user has SSH access to all
target systems, and has fulle `sudo` privileges on those systems.

# Variables used
A couple of variables are used in the playbook, ordered by role:

## setup_ansible
- `ansible_user`
  This role uses the variables `ansible_user` and (optionally) `ansible_ssh_pass`
  to define the user that should be created (and an optional password to go along
  with it).
  *N.B.* This is the same variable that Ansible uses to determine as which user
  to connect over SSH. *DO NOT* set this on the commandline with `-e`, since that
  will also override the variables when the role tries to set them internally.

- `ansible_setup_ssh_key_file`
   The location of the _public_ SSH key that should be added to the
   `authorized_keys` file for the user. Defaults to `~/.ssh/id_rsa.pub`.

## basic_config

- `basic_config_rhel_packages`
  A list of packages to install on every system. Defaults to:
  ```yaml
  basic_config_rhel_packages:
  - tmux
  - vim-enhanced
  - bash-completion
  - firewalld
  ```

- `basic_config_firewall_ports_and_services`
  A list of services and/or ports to open and close on the firewall Every item
  has key named either `service` or `port` that follows the syntax for those
  fields for the Ansible `firewalld` module.  An optional `state` key can be
  added to set the desired state, defaults to `enabled`.
  Defaults:
  ```yaml
  basic_config_firewall_ports_and_services:
  - service: ssh
  - port: 1234/tcp
    state: disabled
  ```

# Disk Layout

This project uses the following files:
```
.
├── hosts                                  # Example inventory file
├── README.md                              # This file
├── roles                                  # Roles used in this project
│   ├── basic_config                       # basic config role
│   │   ├── defaults
│   │   │   └── main.yml
│   │   └── tasks
│   │       ├── enable-firewall.yml
│   │       ├── install_rhel_packages.yml
│   │       └── main.yml
│   └── setup_ansible                      # Role to create ansible user
│       ├── defaults
│       │   └── main.yml
│       └── tasks
│           ├── create_ansible_user.yml
│           └── main.yml
└── site.yml                               # Example playbook using both roles
```
